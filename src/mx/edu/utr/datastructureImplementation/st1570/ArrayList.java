/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructureImplementation.st1570;

import java.util.Arrays;
import mx.edu.utr.datastructures.*;

/**
 *
 * @author EDUARDO
 */
public class ArrayList implements List {

    private Object[] elements;
    private int size;

    public ArrayList() {
        this(10);
    }

    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    /**
     * This method is able to ensure the capacity of my ArrayList
     * @param
     * @return 
     */
    private void enrureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);

        }

    }

    /**
     * This method add an element at the end of my array
     * @param
     * @return 
     */
    @Override
    public boolean add(Object element) {
        enrureCapacity(size + 1);
        elements[size] = element;
        size++;
        return true;
    }

    /**
     * This method add an element to my array in any position
     * @param
     * @return 
     */
    @Override
    public void add(int index, Object element) {
        rangeCheckForAdd(index);
        enrureCapacity(size + 1);
        System.arraycopy(elements, index, elements, index + 1, size - index);
        elements[index] = element;
        size++;
        
    }

    /**
     * This method clear all the array, so it becomes null in all of its values
     * @param
     * @return 
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
            size = 0;
        }
    }

    /**
     * This method show us a value that we want, that values is searched by index
     * @param
     * @return 
     */
    @Override
    public Object get(int index) {
        outOfBound(index);
        return elements[index];
    }

    /**
     * This method is able to show us the position of a value if it is not exists, it return a negative value
     * @param
     * @return 
     */
    @Override
    public int indexOf(Object element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])) {
                    return i;
                }
            }
        }

        return -1;
    }

    /**
     * This method just valid if my array is empty or not
     * @param
     * @return 
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * This method as the name says, removes an element of the arraylist
     * @param
     * @return 
     */
    @Override
    public Object remove(int index) {
        outOfBound(index);
        Object oldElement = elements[index];
        
        int numberMoved = size - index - 1;
        if(numberMoved > 0){
            System.arraycopy(elements, index +1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;
    }

    /**
     * This method overwrites a new values in a specific position
     * @param
     * @return 
     */
    @Override
    public Object set(int index, Object element) {
        outOfBound(index);
        Object old = elements[index];
        elements[index] = element;
        return old;
    }

    /**
     * This method is ensure the size of my arraylost
     * @param
     * @return 
     */
    @Override
    public int size() {
        return size;
    }

    private void rangeCheckForAdd(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void outOfBound(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("Out of Bound");
        }
    }

}
